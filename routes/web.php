<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');

Route::get('/', function () {
    return view('home');
});
Route::get('register', function () {
    return view('form');
});
Route::get('welcome', function () {
    return view('selamatDatang');
});*/

Route::get('/', 'HomeController@home');
Route::get('register', 'AuthController@register');
Route::get('welcome', 'AuthController@selamatDatang');
Route::post('welcome', 'AuthController@selamatDatang_post');
Route::get('master', function(){
    return view('adminlte.master');
});
Route::get('items', function(){
    return view('items.index');
});
Route::get('table', function(){
    return view('adminlte.table');
});
Route::get('data-table', function(){
    return view('adminlte.data-tables');
});