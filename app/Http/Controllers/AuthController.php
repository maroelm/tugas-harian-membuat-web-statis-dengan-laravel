<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register(){
        return view('register');
    }
    public function selamatDatang(){
        return view('selamatDatang');
    }
    public function selamatDatang_post(Request $request){
        //dd($request->all());
        $First_name = $request['First_name'];
        $Last_name = $request['Last_name'];
        $nama_lengkap = $First_name." ".$Last_name;
        return view('selamatDatang',['nama_lengkap'=>$nama_lengkap]);
    }
}
