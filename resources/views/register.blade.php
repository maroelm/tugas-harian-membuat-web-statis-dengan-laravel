<!DOCTYPE html>
<html>
<head>
  <title>Form Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
    @csrf
        <p>
            <label>First name:</label><br>
            <input type="text" name="First name"/>
        </p>
        <p>
            <label>Last name:</label><br>
            <input type="text" name="Last name"/>
        </p>
        <p>
            <label>Gender:</label><br>
            <label><input type="radio" name="Gender" value="Male"/>Male</label><br>
            <label><input type="radio" name="Gender" value="Female"/>Female</label><br>
            <label><input type="radio" name="Gender" value="Other"/>Other</label>
        </p>
        <p>
            <label>Nationality:</label>
            <select name="Nationality"><br>
                <option value="Indonesian">Indonesian</option>
                <option value="Malaysian">Malaysian</option>
                <option value="English">English</option>
                <option value="Etc">Etc</option>
            </select>
        </p>
        <p>
            <label>Language Spoken</label><br>
                <input type="checkbox" value="Bahasa Indonesia">Bahasa Indonesia<br>
                <input type="checkbox" value="English">English<br>
                <input type="checkbox" value="Other">Other<br>
        </p>
        <p>
            <label>Bio:</label><br>
                <textarea name="Bio" cols="25" row="10"></textarea>
        </p>
        <p>
            <input type="submit" name="submit" value="Sign Up"/>
        </p>
</body>
</html>